let dateArray = prompt("Enter your date like 'dd.mm.yyyy.", '01.01.1900').split('.');

let usersBirth = new Date(dateArray[2], dateArray[1] - 1, dateArray[0]); // "-1" потому что работа с форматом даты

function getUsersAge(usersBirth) {
    let today = new Date();
    let age = today.getFullYear() - usersBirth.getFullYear();
    let m = today.getMonth() - usersBirth.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < usersBirth.getDate())) {
        age--;
    }
    return age;
}
alert(`Ваш возраст: ${getUsersAge(usersBirth)} !`);

let month = dateArray[1]; // тут без минуса, так как интересует только число введенное пользователем.
let day = dateArray[0];


let zodiacSigns = {
    capricorn: 'Козерог',
    aquarius: 'Водолей',
    pisces: 'Рыбы',
    aries: 'Овен',
    taurus: 'Телец',
    gemini: 'Близнецы',
    cancer: 'Рак',
    leo: 'Лев',
    virgo: 'Девы',
    libra: 'Весы',
    scorpio: 'Скорпион',
    sagittarius: 'Стрелец'
};

function getZodiacSign(day, month) {
    if ((month == 1 && day <= 20) || (month == 12 && day >= 22)) {
        return zodiacSigns.capricorn;
    } else if ((month == 1 && day >= 21) || (month == 2 && day <= 18)) {
        return zodiacSigns.aquarius;
    } else if ((month == 2 && day >= 19) || (month == 3 && day <= 20)) {
        return zodiacSigns.pisces;
    } else if ((month == 3 && day >= 21) || (month == 4 && day <= 20)) {
        return zodiacSigns.aries;
    } else if ((month == 4 && day >= 21) || (month == 5 && day <= 20)) {
        return zodiacSigns.taurus;
    } else if ((month == 5 && day >= 21) || (month == 6 && day <= 20)) {
        return zodiacSigns.gemini;
    } else if ((month == 6 && day >= 22) || (month == 7 && day <= 22)) {
        return zodiacSigns.cancer;
    } else if ((month == 7 && day >= 23) || (month == 8 && day <= 23)) {
        return zodiacSigns.leo;
    } else if ((month == 8 && day >= 24) || (month == 9 && day <= 23)) {
        return zodiacSigns.virgo;
    } else if ((month == 9 && day >= 24) || (month == 10 && day <= 23)) {
        return zodiacSigns.libra;
    } else if ((month == 10 && day >= 24) || (month == 11 && day <= 22)) {
        return zodiacSigns.scorpio;
    } else if ((month == 11 && day >= 23) || (month == 12 && day <= 21)) {
        return zodiacSigns.sagittarius;
    }
};
alert(`Ваш знак зодиака: ${getZodiacSign(day, month)}`);
