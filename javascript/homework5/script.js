let originObj = {
    state: 'Ukraine',
    capital: 'Kiev',
    population: {
        inGeneral: 44,
        atCapital: {
            legal: 2,
            illegal: 2,
        }
    },
    people: 'nice and friendly',
    someKindOfAnArray: [1, 2, 3, 44],
};

function cloneObj(obj) {
    let objClone = {};
    for (let key in obj) {
        if (obj[key] == true && typeof (obj[key]) == 'object') {
            return objClone[key] = cloneObj(obj[key]);
        }
        else {
            objClone[key] = obj[key];
        }
    }
    return objClone;
}

let obj2 = cloneObj(originObj);
console.log(obj2);

